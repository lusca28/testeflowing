package br.com.alcheno.testeflowing.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.com.alcheno.testeflowing.R;

/**
 * Created by lusca on 16/04/2016.
 */
public class CardHolderPrograms extends RecyclerView.ViewHolder {

    protected TextView titulo, descricao, nome, signature;
    protected ImageView photo;
    protected ImageView relative;
    protected ImageView imageFundo, imageFundo2;

    public CardHolderPrograms(View v) {
        super(v);
        titulo =  (TextView) v.findViewById(R.id.titulo);
        photo = (ImageView)  v.findViewById(R.id.photo);
        nome = (TextView)  v.findViewById(R.id.nome);
        descricao = (TextView)  v.findViewById(R.id.descricao);
        signature = (TextView)  v.findViewById(R.id.signature);
        relative = (ImageView) v.findViewById(R.id.relative);
        imageFundo = (ImageView) v.findViewById(R.id.imageFundo);
        imageFundo2 = (ImageView) v.findViewById(R.id.imageFundo2);
    }
}
