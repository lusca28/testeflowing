package br.com.alcheno.testeflowing.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import br.com.alcheno.testeflowing.R;
import br.com.alcheno.testeflowing.to.TOAvatar;
import br.com.alcheno.testeflowing.to.TOPrograma;

/**
 * Created by lusca on 16/04/2016.
 */
public class CardAdapterPrograms extends RecyclerView.Adapter<CardHolderPrograms> {

    private List<TOPrograma> programaList;
    private Activity context;
    private ImageLoader imageLoader;
    private String color;
    private List<CardHolderPrograms> cardHolderList = new ArrayList<>();

    public CardAdapterPrograms(List<TOPrograma> avatarList, Activity context, ImageLoader imageLoader, String color) {
        this.programaList = avatarList;
        this.context = context;
        this.imageLoader = imageLoader;
        this.color = color;
    }

    @Override
    public int getItemCount() {
        return programaList.size();
    }

    @Override
    public CardHolderPrograms onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_programs, parent, false);

        final CardHolderPrograms viewHolder = new CardHolderPrograms(itemView);
        cardHolderList.add(viewHolder);

        return new CardHolderPrograms(itemView);
    }

    @Override
    public void onBindViewHolder(final CardHolderPrograms holder, int position) {
        final TOPrograma al = programaList.get(holder.getAdapterPosition());
        holder.titulo.setText(al.getProgramName());
        holder.descricao.setText(al.getProgramShortDesciption());
        holder.nome.setText(al.getPartnerName());
        holder.signature.setText(al.getPartnerSignature());
        imageLoader.displayImage(al.getPartnerProfilePicture(), holder.photo);
        holder.relative.setBackgroundColor(Color.parseColor(color));
        imageLoader.displayImage(al.getProgramRetinaImage_wide(), holder.imageFundo);
        holder.imageFundo2.setBackgroundColor(Color.parseColor(color));
    }
}
