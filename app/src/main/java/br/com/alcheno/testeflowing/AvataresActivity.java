package br.com.alcheno.testeflowing;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.lang.reflect.Type;
import java.util.List;

import br.com.alcheno.testeflowing.adapter.CardAdapter;
import br.com.alcheno.testeflowing.to.TOAvatar;
import br.com.alcheno.testeflowing.to.TOLogin;

/**
 * Created by lusca on 09/05/2016.
 */
public class AvataresActivity extends AppCompatActivity {

    private TOLogin toLogin;
    private RecyclerView cardList;
    private CardAdapter adapter;
    private ImageLoader imageLoader;
    private Button btProximo;
    private TextView msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avatares);

        if (getIntent().getExtras() != null) {
            Bundle b = getIntent().getExtras();
            toLogin = (TOLogin) b.getSerializable("login");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.parseColor(toLogin.getCompanyPrimaryColor()));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        btProximo = (Button) findViewById(R.id.btProximo);
        cardList = (RecyclerView) findViewById(R.id.cardList);
        msg = (TextView) findViewById(R.id.msg);

        cardList.setHasFixedSize(true);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(AvataresActivity.this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        cardList.setLayoutManager(mLayoutManager);

        //imagens
        DisplayImageOptions opts = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(opts)
                .threadPriority(Thread.MIN_PRIORITY)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        //busca a lista
        IInterface iInterface = new IInterface() {
            @Override
            public void processFinish(Object output) {
                JsonParser jsonParser = new JsonParser();
                JsonArray gsonObj = jsonParser.parse(output.toString()).getAsJsonArray();

                Type listOfTestObject = new TypeToken<List<TOAvatar>>() {}.getType();
                Gson gson = new Gson();
                List<TOAvatar> avatares = gson.fromJson(gsonObj, listOfTestObject);
                adapter = new CardAdapter(avatares, AvataresActivity.this, imageLoader, toLogin.getCompanyPrimaryColor());
                cardList.setAdapter(adapter);
            }

            @Override
            public void processFinishErro(String output) {
                Toast.makeText(AvataresActivity.this, output, Toast.LENGTH_LONG).show();
            }
        };
        HttpTask task = new HttpTask(AvataresActivity.this, iInterface, toLogin.getAccess_token(), getString(R.string.link_avatares));
        task.execute();

        //bt proximo
        btProximo.setBackgroundColor(Color.parseColor(toLogin.getCompanyPrimaryColor()));
        //TODO fazer uma logica para que se vier cor escura a cor do texto ser branco
        btProximo.setTextColor(Color.parseColor("#FFFFFF"));
        btProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapter.isClicado()) {
                    msg.setVisibility(View.GONE);

                    Intent it = new Intent(AvataresActivity.this, ProgramasActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("login", toLogin);
                    it.putExtras(bundle);
                    startActivity(it);
                } else {
                    msg.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            Intent it = new Intent(AvataresActivity.this, LoginActivity.class);
            startActivity(it);

            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
