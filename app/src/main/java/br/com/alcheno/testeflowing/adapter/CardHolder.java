package br.com.alcheno.testeflowing.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import br.com.alcheno.testeflowing.R;

/**
 * Created by lusca on 16/04/2016.
 */
public class CardHolder extends RecyclerView.ViewHolder {

    protected RadioButton radio;
    protected ImageView avatar;
    protected TextView nome, texto;

    public CardHolder(View v) {
        super(v);
        radio =  (RadioButton) v.findViewById(R.id.radio);
        avatar = (ImageView)  v.findViewById(R.id.avatar);
        nome = (TextView)  v.findViewById(R.id.nome);
        texto = (TextView)  v.findViewById(R.id.texto);
    }
}
