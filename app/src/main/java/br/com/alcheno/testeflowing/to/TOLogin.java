package br.com.alcheno.testeflowing.to;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by lusca on 09/05/2016.
 */
public class TOLogin implements Serializable {

    private String access_token;
    private String token_type;
    private String expires_in;
    private String FullName;
    private String Company;
    private String CompanyLogoPath;
    private String CompanyPrimaryColor;
    private Date issued;
    private Date expires;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getCompanyLogoPath() {
        return CompanyLogoPath;
    }

    public void setCompanyLogoPath(String companyLogoPath) {
        CompanyLogoPath = companyLogoPath;
    }

    public String getCompanyPrimaryColor() {
        return CompanyPrimaryColor;
    }

    public void setCompanyPrimaryColor(String companyPrimaryColor) {
        CompanyPrimaryColor = companyPrimaryColor;
    }

    public Date getIssued() {
        return issued;
    }

    public void setIssued(Date issued) {
        this.issued = issued;
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }
}
