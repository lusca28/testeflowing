package br.com.alcheno.testeflowing.to;

/**
 * Created by lusca on 09/05/2016.
 */
public class TOAvatar {

    private int AvatarId;
    private String AvatarName;
    private String AvatarBio;
    private String AvatarPicturePath;
    private boolean IsSelected;

    public int getAvatarId() {
        return AvatarId;
    }

    public void setAvatarId(int avatarId) {
        AvatarId = avatarId;
    }

    public String getAvatarName() {
        return AvatarName;
    }

    public void setAvatarName(String avatarName) {
        AvatarName = avatarName;
    }

    public String getAvatarBio() {
        return AvatarBio;
    }

    public void setAvatarBio(String avatarBio) {
        AvatarBio = avatarBio;
    }

    public String getAvatarPicturePath() {
        return AvatarPicturePath;
    }

    public void setAvatarPicturePath(String avatarPicturePath) {
        AvatarPicturePath = avatarPicturePath;
    }

    public boolean isSelected() {
        return IsSelected;
    }

    public void setIsSelected(boolean isSelected) {
        IsSelected = isSelected;
    }
}
