package br.com.alcheno.testeflowing.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import br.com.alcheno.testeflowing.R;
import br.com.alcheno.testeflowing.to.TOAvatar;

/**
 * Created by lusca on 16/04/2016.
 */
public class CardAdapter extends RecyclerView.Adapter<CardHolder> {

    private List<TOAvatar> avatarList;
    private Activity context;
    private ImageLoader imageLoader;
    private String color;
    private List<CardHolder> cardHolderList = new ArrayList<>();
    private boolean clicado = false;

    public CardAdapter(List<TOAvatar> avatarList, Activity context, ImageLoader imageLoader, String color) {
        this.avatarList = avatarList;
        this.context = context;
        this.imageLoader = imageLoader;
        this.color = color;
    }

    @Override
    public int getItemCount() {
        return avatarList.size();
    }

    @Override
    public CardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);

        final CardHolder viewHolder = new CardHolder(itemView);
        cardHolderList.add(viewHolder);

        return new CardHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CardHolder holder, int position) {
        final TOAvatar al = avatarList.get(position);
        holder.nome.setText(al.getAvatarName());
        holder.nome.setTextColor(Color.parseColor(color));
        holder.texto.setText(al.getAvatarBio());
        holder.radio.setChecked(al.isSelected());

        holder.radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.radio.isChecked()) {
                    holder.radio.setChecked(false);
                } else {
                    for (int i = 0; i < cardHolderList.size(); i++) {
                        cardHolderList.get(i).radio.setChecked(false);
                    }
                    holder.radio.setChecked(true);
                    clicado = true;
                }
            }
        });

        imageLoader.displayImage(al.getAvatarPicturePath(), holder.avatar);
    }

    public boolean isClicado() {
        return clicado;
    }
}
