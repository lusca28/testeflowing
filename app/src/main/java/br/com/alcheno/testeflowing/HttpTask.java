package br.com.alcheno.testeflowing;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class HttpTask extends AsyncTask<String, Void, Object> {

    private volatile boolean running = true;
    private IInterface service;
    private boolean problema;
    private String msgErro, link, accessToken;
    private List<NameValuePair> nameValuePairs;
    private ProgressDialog progressDialog;
    private Activity activity;
    private boolean login;
    private HttpGet httpGet;
    private HttpPost httpPost;

    public HttpTask(Activity activity, IInterface service, List<NameValuePair> nameValuePairs, String link) {
        this.service = service;
        this.nameValuePairs = nameValuePairs;
        this.link = link;
        this.activity = activity;
        this.login = true;
    }

    public HttpTask(Activity activity, IInterface service, String accessToken, String link) {
        this.service = service;
        this.accessToken = accessToken;
        this.link = link;
        this.activity = activity;
        this.login = false;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = ProgressDialog.show(
                activity, null, "Carregando...", true, true, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        activity.finish();
                    }
                });
        progressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    protected Object doInBackground(String... args) {

        while (running) {
            Object sucesso = null;
            HttpClient httpClient = new DefaultHttpClient();
            if (!login){
                httpGet = new HttpGet(link);
                httpGet.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=UTF-8");
                httpGet.setHeader("Authorization", "Bearer " + accessToken);

            } else {
                httpPost = new HttpPost(link);
                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=UTF-8");

                try {
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            try {
                HttpResponse response = null;
                if (login) {
                    response = httpClient.execute(httpPost);
                } else {
                    response = httpClient.execute(httpGet);
                }

                HttpEntity entity = response.getEntity();
                if (response.getStatusLine().getStatusCode() == 200) {
                    if (entity != null) {
                        String retSrc = EntityUtils.toString(entity);

                        JsonParser parser = new JsonParser();
                        JsonObject resultObj = null;
                        JsonArray resultArray = null;

                        try {
                            resultObj = parser.parse(retSrc).getAsJsonObject();
                            sucesso = resultObj;
                        } catch (Exception ex){
                            resultArray = parser.parse(retSrc).getAsJsonArray();
                            sucesso = resultArray;
                        }
                    }
                }
            } catch (Exception ex) {
                problema = true;
                msgErro = "catch: " + ex.getMessage();
            } finally {
                httpClient.getConnectionManager().shutdown();
            }

            return sucesso;
        }

        return null;
    }

    protected void onPostExecute(Object result) {
        if (problema) {
            service.processFinishErro(msgErro);
        } else {
            service.processFinish(result);
        }
        progressDialog.dismiss();
    }
}

