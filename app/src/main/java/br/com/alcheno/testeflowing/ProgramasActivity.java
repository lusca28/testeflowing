package br.com.alcheno.testeflowing;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.alcheno.testeflowing.adapter.CardAdapter;
import br.com.alcheno.testeflowing.adapter.CardAdapterPrograms;
import br.com.alcheno.testeflowing.to.TOAvatar;
import br.com.alcheno.testeflowing.to.TOLogin;
import br.com.alcheno.testeflowing.to.TOPrograma;

/**
 * Created by lusca on 10/05/2016.
 */
public class ProgramasActivity extends AppCompatActivity {

    private TOLogin toLogin;
    private RecyclerView cardList;
    private ImageLoader imageLoader;
    private CardAdapterPrograms adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programs);

        if (getIntent().getExtras() != null) {
            Bundle b = getIntent().getExtras();
            toLogin = (TOLogin) b.getSerializable("login");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.parseColor(toLogin.getCompanyPrimaryColor()));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cardList = (RecyclerView) findViewById(R.id.cardList);
        cardList.setHasFixedSize(true);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(ProgramasActivity.this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        cardList.setLayoutManager(mLayoutManager);

        //imagens
        DisplayImageOptions opts = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .displayer(new RoundedBitmapDisplayer(50))
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(opts)
                .threadPriority(Thread.MIN_PRIORITY)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        //busca a lista
        IInterface iInterface = new IInterface() {
            @Override
            public void processFinish(Object output) {
                JsonParser jsonParser = new JsonParser();
                JsonArray gsonObj = jsonParser.parse(output.toString()).getAsJsonArray();

                Type listOfTestObject = new TypeToken<List<TOPrograma>>() {}.getType();
                Gson gson = new Gson();
                List<TOPrograma> programas = gson.fromJson(gsonObj, listOfTestObject);
                adapter = new CardAdapterPrograms(programas, ProgramasActivity.this, imageLoader, toLogin.getCompanyPrimaryColor());
                cardList.setAdapter(adapter);
            }

            @Override
            public void processFinishErro(String output) {
                Toast.makeText(ProgramasActivity.this, output, Toast.LENGTH_LONG).show();
            }
        };
        HttpTask task = new HttpTask(ProgramasActivity.this, iInterface, toLogin.getAccess_token(), getString(R.string.link_programs));
        task.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

            Intent it = new Intent(ProgramasActivity.this, AvataresActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("login", toLogin);
            it.putExtras(bundle);
            startActivity(it);

            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
