package br.com.alcheno.testeflowing.to;

/**
 * Created by lusca on 10/05/2016.
 */
public class TOPrograma {

    private int RecomendationRate;
    private boolean SignedIn;
    private int TotalParticipants;
    private int ProgramId;
    private int UserId;
    private String PartnerDescription;
    private String PartnerProfilePicture;
    private String PartnerSignature;
    private String PartnerName;
    private String ProgramRetinaImage_wide;
    private String ProgramImage_wide;
    private String ProgramRetinaImage_square;
    private String ProgramImage_square;
    private String ProgramShortDesciption;
    private String ProgramDescription;
    private String ProgramType;
    private String ProgramTypeId;
    private String ProgramName;
    private String UserName;

    public int getRecomendationRate() {
        return RecomendationRate;
    }

    public void setRecomendationRate(int recomendationRate) {
        RecomendationRate = recomendationRate;
    }

    public boolean isSignedIn() {
        return SignedIn;
    }

    public void setSignedIn(boolean signedIn) {
        SignedIn = signedIn;
    }

    public int getTotalParticipants() {
        return TotalParticipants;
    }

    public void setTotalParticipants(int totalParticipants) {
        TotalParticipants = totalParticipants;
    }

    public int getProgramId() {
        return ProgramId;
    }

    public void setProgramId(int programId) {
        ProgramId = programId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getPartnerDescription() {
        return PartnerDescription;
    }

    public void setPartnerDescription(String partnerDescription) {
        PartnerDescription = partnerDescription;
    }

    public String getPartnerProfilePicture() {
        return PartnerProfilePicture;
    }

    public void setPartnerProfilePicture(String partnerProfilePicture) {
        PartnerProfilePicture = partnerProfilePicture;
    }

    public String getPartnerSignature() {
        return PartnerSignature;
    }

    public void setPartnerSignature(String partnerSignature) {
        PartnerSignature = partnerSignature;
    }

    public String getPartnerName() {
        return PartnerName;
    }

    public void setPartnerName(String partnerName) {
        PartnerName = partnerName;
    }

    public String getProgramRetinaImage_wide() {
        return ProgramRetinaImage_wide;
    }

    public void setProgramRetinaImage_wide(String programRetinaImage_wide) {
        ProgramRetinaImage_wide = programRetinaImage_wide;
    }

    public String getProgramImage_wide() {
        return ProgramImage_wide;
    }

    public void setProgramImage_wide(String programImage_wide) {
        ProgramImage_wide = programImage_wide;
    }

    public String getProgramRetinaImage_square() {
        return ProgramRetinaImage_square;
    }

    public void setProgramRetinaImage_square(String programRetinaImage_square) {
        ProgramRetinaImage_square = programRetinaImage_square;
    }

    public String getProgramImage_square() {
        return ProgramImage_square;
    }

    public void setProgramImage_square(String programImage_square) {
        ProgramImage_square = programImage_square;
    }

    public String getProgramShortDesciption() {
        return ProgramShortDesciption;
    }

    public void setProgramShortDesciption(String programShortDesciption) {
        ProgramShortDesciption = programShortDesciption;
    }

    public String getProgramDescription() {
        return ProgramDescription;
    }

    public void setProgramDescription(String programDescription) {
        ProgramDescription = programDescription;
    }

    public String getProgramType() {
        return ProgramType;
    }

    public void setProgramType(String programType) {
        ProgramType = programType;
    }

    public String getProgramTypeId() {
        return ProgramTypeId;
    }

    public void setProgramTypeId(String programTypeId) {
        ProgramTypeId = programTypeId;
    }

    public String getProgramName() {
        return ProgramName;
    }

    public void setProgramName(String programName) {
        ProgramName = programName;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }
}
